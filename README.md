# EMacquisitionMacro

Code for EM stack acquisition part of the following paper:
"Neuropathic pain caused by mis-wiring and abnormal end organ targeting", Gangadharan et al 2022


## Authors

This package was developed by
* **Florian Drawitsch** 
* **Ali Karimi** 
* **Jakob Straehle** 

under scientific supervision of
* **Moritz Helmstaedter**

## License
This project is licensed under the [MIT license](LICENSE).  
Copyright (c) 2022 Department of Connectomics, Max Planck Institute for
Brain Research, D-60438 Frankfurt am Main, Germany

