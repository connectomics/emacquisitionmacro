%% Your Input, Stackinfo
close all;
C = struct;
F = struct;
detectBVNuclei = true;
detectIID =true;
graphing=true;
C.sliceStart = 1;%last slice with AF done plus 1,make it also 1 in the config.txt
C.AFcolNr = 2;% number of Focus Columns DO NOT change for now % 1 if only one Motortile, otherwise set to 2
C.AFrowNr = 2;% number of focus rows DO NOT change for now % 1 if only one Motortile, otherwise set to 2
C.onlyOneTile = false% true; % in addtion to changing this to false also put C.AFcolNr and C.AFrowNr back to 2.
numberOfCores = 8;
parentDir = 'X:\stacks'; % Dont change! Verios = 'X:\stacks'
matlabFolder = 'X:\acquisitionMacro\matlab';
addpath(genpath(fullfile(matlabFolder,'Autofocus')));%directory of the Autofocus in the acquisition Macro

%% Preparations

C = configReader(matlabFolder,C);
% Mask creation for AFcoeff calculation
stigMasks = makeStigMasks(6, 0.5, 3, 64);
focusMasks = makeFocusMasks(9, 0.5, 3, 64);
stigMaskArray = repmat(stigMasks,[C.mc C.mr]);
focusMasksArray = repmat(focusMasks,[C.mc C.mr]);

% Parameters for chopping the FOV into focus tiles
borderCol =floor(C.mc/C.AFcolNr);
borderRow =floor(C.mr/C.AFrowNr);
% the vectors for chopping up the columns and rows with mat2cell
C.afrows = [borderRow,C.mr-borderRow];
C.afcols = [borderCol,C.mc-borderCol];
C.numberOfTiles= C.mc*C.mr;


% Formatters for making sliceDir and imageName
F.numberformatter = @(x) fullfile(sprintf('%06d',floor(x/100)),sprintf('%06d',x));
F.makeImName = @ (dat,column,row,sliceNr,stackID) [dat,'_',stackID,'_',sprintf('%06.0f',sliceNr),...
    '_',sprintf('%03.0f',column),'_',sprintf('%03.0f',row),'_000_n_00.tif'];
F.formatCelltoArray =@(x,C)reshape((cat(1,x{:})),[C.AFcolNr,C.AFrowNr,3]);

% read the stacknumber From whichstack.txt
[C.stackID, C.stNum ] = readwhichStack(parentDir);

% Create Top Directories
C.stopFname = fullfile(parentDir,['st',sprintf('%03.0f',C.stNum),'Top'],...
    [C.stackID,'_focusstop.txt']);
C.AFDir = fullfile(parentDir,['st',sprintf('%03.0f',C.stNum),'Top'],['st',sprintf('%03.0f',C.stNum),'AF']);
C.ImgDir = fullfile(parentDir,sprintf('%sTop/%s', C.stackID, C.stackID));
C.configDir = fullfile(parentDir,['st',sprintf('%03.0f',C.stNum),'Top'],['st',sprintf('%03.0f',C.stNum),'Config']);
if ~isdir(C.AFDir)
    mkdir(C.AFDir);
end



% Gain parameters plus limits for corrections
focgain = 0.1; %Updated 17.01.2016 with ppcAK0093
stigxgain = 2660; %Updated 17.01.2016 with ppcAK0093 (linear region)
stigygain = 2660; %Updated 14.01.2016 with ppcAK0093 (linear region)
C.gain = [focgain,stigxgain,stigygain];
limits = [20,10,10];% Maximum calculated values in the test images
C.AFlimits = C.gain.*limits;
C.stopLimit=[7.5 7.5 7.5].*C.gain;

%Rotation matrix
defaultRotation = 28.7; %Verios =28.7
C.R = makeRotationMatrix(defaultRotation-2*C.scanRotation);%2 multiplication happens between the autocorrelation to correction change
% IID parameters
IIDflag = false(C.NrofSlices,1);
fractionIIDTiles =0.2;
initialIIDThresh = {35};
initialIIDThreshArray = repmat(initialIIDThresh,[C.mc,C.mr]);
% Graphing
C.NrCorrtectionsGraph = 25;
logDataSlice=zeros(C.mc,C.mr,2,3);
logDataGraph = zeros(C.AFcolNr,C.AFrowNr,2,3,C.NrCorrtectionsGraph);

C.savingIntervalGraph =3;

figureVisibility='On';% Options: 'On', 'Off'
if graphing
    h = figure('Position', [1 1 1920 1124],'Visible',figureVisibility);
end

%Defining variables
coeff =zeros(C.mc,C.mr,3,2);
imageMontage = cell(C.mc,C.mr,4);
allIID = zeros(C.mc,C.mr,C.NrofSlices);
OORflag = false(C.AFcolNr,C.AFrowNr,C.NrofSlices);
DeltaArray= zeros(C.AFcolNr,C.AFrowNr,3,C.NrofSlices);
Delta =cell(C.AFcolNr,C.AFrowNr);


%start parallel pool
if isempty(gcp('nocreate'))
    cluster = parpool ('local',numberOfCores);
    cluster.IdleTimeout = 120;
end
%% Algorithm

%initiating the logFile
t = strcat(datestr(clock,'yyyy-mm-dd-HHMM'));
initiateLog = {[t,': Autofocus started ']};
fid = fopen(fullfile(C.AFDir,'AFlog.txt'),'a');
fprintf(fid,'%s \n',initiateLog{1,1});
fclose(fid);

lastMontageSlice = 1;
%load previous values for continuity plus set the last imaged slice to slice
%start
lastAFSlice=double(counterReader(C));
if lastAFSlice>0
    load(fullfile(C.AFDir,F.numberformatter(lastAFSlice),'parameters'),'coeff','IIDflag','OORflag','logDataGraph',...
        'lastMontageSlice','allIID','Delta','DeltaArray','shiftsBV');%don't load C, it changes the par if changed in the first part of script
    [imageMontage(:,:,1),imageMontage(:,:,3)] = readSliceImgs(lastMontageSlice,C,F);
end
C.lastImagedSlice = C.sliceStart;

while 1
    %last imaged slice
    C.lastImagedSlice = getMostRecentDirAF(C,F);
    %last slice with AF done if start = 0
    lastAFSlice = double(counterReader(C));
    
    if C.lastImagedSlice >= lastAFSlice+1
        
        for thisSlice = lastAFSlice+1:C.lastImagedSlice
            C.thisSlice = thisSlice;% Have this slice in the C struct as well
            if C.thisSlice>C.NrofSlices
                disp ('The AF has reached its maximum number of slices increas C.NrofSlices')
                break;
            else
                
                tic;
                
                sliceLog = {};
                
                
                % Set-up directories for current Slice
                C.thisSliceAFDir = fullfile(C.AFDir,F.numberformatter(C.thisSlice));
                if~isdir(C.thisSliceAFDir)
                    mkdir(C.thisSliceAFDir)
                end
                C.thisSliceImgDir = fullfile(C.ImgDir,F.numberformatter(C.thisSlice));
                C.thisSliceConfigDir = fullfile(C.configDir,F.numberformatter(C.thisSlice));
                
                %Reading the images and filenames of the currentslice into Montage
                [imageMontage(:,:,2),imageMontage(:,:,4)] = readSliceImgs(C.thisSlice,C,F);
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%  Detecting Blood vessels         %%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                if detectBVNuclei
                    %only do BVN and crop location calculation in the third
                    %slice
                    if mod(C.thisSlice,3)==2 || C.thisSlice==C.sliceStart
                        BVN = cell(C.mc,C.mr);
                        for c=1:C.mc
                            BVTemp =cell(1,C.mr);
                            for r =1:C.mr
                                BVTemp{1,r} = detectBVN(imageMontage{c,r,2});
                            end
                            BVN(c,:)=BVTemp;
                        end
                        if mod(C.thisSlice,30)==0
                            createBVNOVerlay(BVN,imageMontage,C);
                        end
                        % shiftBV used for cropping images in measureShiftSURF and getAFcoeff, shift IID raw
                        % and shiftSURF used in measureShiftIID to have registered images
                        [shiftsBV,shiftsIIDRaw] = cellfun(@getCenterShiftBVN,BVN,'UniformOutput',false);
                    end
                else
                    shiftsBV = repmat({[0,0]},[C.mc,C.mr]);
                    shiftsIIDRaw = repmat({[0,0]},[C.mc,C.mr]);
                end
                
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%          Debri Detection         %%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                if detectIID
                    if C.thisSlice>3
                        shiftSURF = cellfun(@measureShiftsSURFIID,imageMontage(:,:,1),imageMontage(:,:,2),shiftsBV,'UniformOutput',false);
                        %set Threshhold based on average and std after 100
                        %slices
                        if C.thisSlice > 100
                            NewthreshArray = getIIDThreshhold(allIID,C);
                            [IID,meanDiff] =cellfun(@measureIID,imageMontage(:,:,1),imageMontage(:,:,2),shiftSURF,shiftsIIDRaw,NewthreshArray,'UniformOutput',false);
                        else
                            [IID,meanDiff] =cellfun(@measureIID,imageMontage(:,:,1),imageMontage(:,:,2),shiftSURF,shiftsIIDRaw,initialIIDThreshArray,'UniformOutput',false);
                        end
                        % changing from cell to array for IID values
                        allIID(:,:,C.thisSlice) = cell2mat(meanDiff);
                        IID =cell2mat(IID);
                        
                        % Threshholding to decide whether tconsider slice
                        % as a debri slice, plus checking whether more than
                        % three slices in a row are detected as debri
                        % slices and resetting if that is the case
                        
                        if sum(IID(:))>=fractionIIDTiles*C.numberOfTiles && sum(IIDflag(C.thisSlice-2:C.thisSlice-1))<2
                            IIDflag(C.thisSlice) = true;
                            sliceLog = sliceLogger( sliceLog,['Debri Detected, Slice:',num2str(C.thisSlice)]);
                        else
                            lastMontageSlice = C.thisSlice;
                            IIDflag(C.thisSlice) =false;
                            imageMontage(:,:,1)=imageMontage(:,:,2);
                            imageMontage(:,:,3)=imageMontage(:,:,4);
                            
                            if sum(IIDflag(C.thisSlice-2:C.thisSlice-1))==2
                                
                                sliceLog = sliceLogger(sliceLog,'IID ignored, After three slices in a row');
                            end
                            
                        end
                    else
                        sliceLog = sliceLogger(sliceLog,'IID skipped, first Slice');
                        imageMontage(:,:,1)=imageMontage(:,:,2);
                        imageMontage(:,:,3)=imageMontage(:,:,4);
                    end
                else
                    sliceLog = sliceLogger(sliceLog,'IID Inactive');
                    imageMontage(:,:,1)=imageMontage(:,:,2);
                    imageMontage(:,:,3)=imageMontage(:,:,4);
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%  Coefficient calculation for a single slice         %%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % cropping images with avoidance of BVs
                sliceImgCrops = cellfun(@cropImage,imageMontage(:,:,2),shiftsBV,'UniformOutput',false);
                if mod(C.thisSlice,1)==0
                    createCropMontage(sliceImgCrops,C);
                end
                % coefficient calculations
                [FocCoeff,stigXcoeff,stigYcoeff] = cellfun(@getAFcoeff,sliceImgCrops,stigMaskArray,focusMasksArray,'UniformOutput',false);
                % added coefficient to the new slice value (2) in coeff variables
                coeff(:,:,:,2) = cat(3,cell2mat(FocCoeff),cell2mat(stigXcoeff),cell2mat(stigYcoeff));
                % Update Log
                sliceLog= sliceLogger(sliceLog, ['slice: ', sprintf('%d',C.thisSlice),' Autofocus Coefficient Calculations done!']);
                
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%  calculation of Focus/Stigmation Corrections for slice+2   %%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                
                if (C.thisSlice>3 && mod(C.thisSlice,3)==1)
                    if ~IIDflag(C.thisSlice) && ~IIDflag(C.thisSlice-1)
                        % Calculating the correction values
                        [Delta,OORflagcell,stopFlagForTile] = getDeltaSlice(coeff,C);
                        if sum([stopFlagForTile{:}])>=0.5*C.AFcolNr*C.AFrowNr
                            fid=fopen(C.stopFname,'w');fclose(fid);
                            disp(['slice ',num2str(C.thisSlice),': ',...
                            'stop File created because of stop limits being reached']);
                        end
                        OORflag(:,:,C.thisSlice)= cell2mat(OORflagcell);
                        AFDeltaWriter(Delta,C,F);
                        DeltaArray(:,:,:,C.thisSlice+2) = F.formatCelltoArray(Delta,C);
                        %Updating Log
                        sliceLog = sliceLogger(sliceLog,['focus corrections for',' Slice ',num2str(C.thisSlice+2) ' are:' ]);
                        for i =1:C.AFcolNr
                            for j=1:C.AFrowNr
                                sliceLog = sliceLogger(sliceLog, [' AFCol: ',num2str(j),' AFRow: ',num2str(i),' Focus: ',num2str(Delta{i,j}(1)),...
                                    ' StigX: ',num2str(Delta{i,j}(2)),' Stigy: ',num2str(Delta{i,j}(3))]);
                            end
                        end
                        %Mentioning motortiles skipped due to reaching the
                        %correction limit
                        sliceLog = sliceLogger(sliceLog, ['focus corrections skipped in ',num2str(sum(sum(OORflag(:,:,C.thisSlice)))) ' aftiles due to being out of range' ]);
                    else
                        %setting all correction values to zero in case of debri
                        
                        [Delta, OORflag(:,:,C.thisSlice)] = getDeltaSlice(coeff,C,1);
                        AFDeltaWriter(Delta,C,F);
                        sliceLog = sliceLogger(sliceLog,['focus corrections skipped in',' Slice ',num2str(C.thisSlice+2)...
                            ' due to debri in one of these slices:',num2str(C.thisSlice),', ',num2str(C.thisSlice-1) ]);
                    end
                end
                
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%                             Graphing                       %%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %Convention: "Delta" is the correction calculated by this script and
                %"correction" is read from the Autofocus.java
                if graphing && C.thisSlice>3 &&  mod(C.thisSlice,3)==0
                    if exist(fullfile(C.thisSliceConfigDir,'af_log.txt'),'file')==2
                        logDataSlice = focusLogReader(C,'af_log.txt');%logDataSlice(column,row,absolute/correction,Foc/stigX/stigY)
                    else
                        logDataSlice = focusLogReader(C,'focus_log_new.txt');
                        sliceLog =  sliceLogger(sliceLog,'Manual Focusing done on this Slice');
                    end
                    if C.thisSlice<=(C.NrCorrtectionsGraph*3)+3
                        if C.onlyOneTile
                            logDataGraph(:,:,:,:,(C.thisSlice/3)-1) = logDataSlice(1,1,1:2,1:3);
                        else
                            logDataGraph(:,:,:,:,(C.thisSlice/3)-1) = logDataSlice(borderCol:borderCol+1,borderRow:borderRow+1,1:2,1:3);
                        end
                    else
                        logDataGraph(:,:,:,:,1:C.NrCorrtectionsGraph-1)= logDataGraph(:,:,:,:,2:C.NrCorrtectionsGraph);
                        if C.onlyOneTile
                             logDataGraph(:,:,:,:,C.NrCorrtectionsGraph)=logDataSlice(1,1,1:2,1:3);
                        else
                            logDataGraph(:,:,:,:,C.NrCorrtectionsGraph)=logDataSlice(borderCol:borderCol+1,borderRow:borderRow+1,1:2,1:3);
                        end
                    end
                    
                    if ishandle(h)
                        createAFgraph(logDataGraph,C,h);
                    else
                        h=figure();
                        createAFgraph(logDataGraph,C,h);
                    end
                    
                end
                
                
                disp(sliceLog);
                % Write the slice log in the logfifDEle
                fid = fopen(fullfile(C.AFDir,'AFlog.txt'),'a');
                for i = 1:size(sliceLog,1)
                    fprintf(fid,'%s \r\n',sliceLog{i,:});
                end
                fclose(fid);
                
                %saving coefficients plus IID flags in AFdir, AFcoeff
                %contains the same values(thisSlice) at both position,
                %the second row in the 4th dimension updated with new slice
                %in the next round of the for loop
                coeff(:,:,:,1)= coeff(:,:,:,2);
                % saving parameters in mat file
                save(fullfile(C.thisSliceAFDir,'parameters'),'coeff','IIDflag','OORflag','logDataGraph',...
                    'lastMontageSlice','allIID','Delta','DeltaArray','shiftsBV','C');
                % Updating the counter
                counterWriter(C);
                toc;
            end
        end
    end
    pause(20)
end

