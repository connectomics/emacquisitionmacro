% Universal Stack Monitor
%
%  florian.drawitsch@brain.mpg.de
%  v160916


%% Parameters
% General Parameters
addpath(fullfile(pwd,'monitorFunctions'));
stacksDir = 'X:\stacks\';
[stackID,~] = readwhichStack(stacksDir); 
stackDirTop = fullfile(stacksDir,[stackID,'Top']);

stopFileTargetDir = stackDirTop;
tableauRefImgPath = 'X:\screening\JS\refimage.tif';
sliceStart = 1;
numberOfTiles = 24;

% Email Parameters
emailSendInt = 25; % Email Update Interval in slices
emailRecipients = {'vijayan.gangadharan@brain.mpg.de'};
emailStopSubject = 'stop';
emailStartSubject = 'start';

% Cutting Test Parameters
zAlignNumberOfSlices = 10;
zAlignTileRow = 2;
zAlignTileColumn = 1;
zAlignCropSize = 500;

% Failsafe system Parameters
failsafeActive = 1;
% minimally needed normalized pixel-wise difference for alarm not to go off
threshIdentity = 25;
% minimally needed image entropy for alarm not to go off
threshEntropy = 6;



%% Initialize
% Set up Output Folder
outputDir = fullfile(stackDirTop,[stackID,'Monitor']);
if ~exist(outputDir), mkdir(outputDir), end;

%AFDir 
AFDir = fullfile(stackDirTop,[stackID,'AF']);

% Stop File
stopFileFnameEmail = fullfile(stopFileTargetDir,[stackID,'_stopEmail.txt']);
stopFileFnameCheck = fullfile(stopFileTargetDir,[stackID,'_stopCheck.txt']);
stopFileFnameFocusMonitor = fullfile(stopFileTargetDir,[stackID,'_focusstop.txt']);
% Configure Email
setpref('Internet', 'E_mail', 'utom2you3@gmail.com');
setpref('Internet', 'SMTP_Username', 'utom2you3@gmail.com');
setpref('Internet', 'SMTP_Password', 'mhlabbrains');
setpref('Internet', 'SMTP_Server', 'smtp.gmail.com');
props = java.lang.System.getProperties;
props.setProperty('mail.smtp.auth','true');
props.setProperty('mail.smtp.socketFactory.class', 'javax.net.ssl.SSLSocketFactory');
props.setProperty('mail.smtp.socketFactory.port', '465');

% Set up number formatter
numberformatter = @(x) sprintf('%06d/%06d', [floor(x/100),  x ]);

% Start Monitoring
t = strcat(datestr(clock,'yyyy-mm-dd-HHMM'));
GLOBLOG = {[t,': Stack monitoring started ']};
fid = fopen(fullfile(outputDir,'_log.txt'),'a');
fprintf(fid,'%s \n',GLOBLOG{1,1});
fclose(fid);
stopFlagEmail = 0;
stopFlagCheck = 0;
lastSlice = 0;
lastSliceEmail = 0;
waitOverride = 0;
thisSlice = sliceStart;

[ thisSliceSmallDir, thisSlice ] = getMostRecentDir( thisSlice, fullfile(stackDirTop,stackID), numberOfTiles);
disp(['Starting Monitoring at slice ', num2str(thisSlice)]);

LOCLOG={};
%% Exectute Monitoring Loop
while true
    try
        %reset stop file check
        stopFlagCheck =0;
        % Check most recently written dir
        [ thisSliceSmallDir, thisSlice ] = getMostRecentDir( thisSlice, fullfile(stackDirTop,stackID), numberOfTiles);
        
        % Check Email
%         [ stopFlagEmail, msgEmail ] = readMail( emailRecipients, emailStartSubject, emailStopSubject, stopFlagEmail );
        
        stopFlagEmail = 0;
        msgEmail = char('No email recieved');
        
        % Create / Remove StopFile depending on stopFlag
        if stopFlagEmail == 1
            if ~exist(stopFileFnameEmail,'file')>0
                fid = fopen(stopFileFnameEmail,'w'); fclose(fid);
                sendmail(emailRecipients, ...
                    ['Universal Monitor Tool: Stop Mail Received. STOPPED (z=',sprintf('%04.0f',thisSlice),')'], ...
                    strcat(LOCLOG,';'));
            end
        else
            if exist(stopFileFnameEmail,'file')>0
                delete(stopFileFnameEmail);
                sendmail(emailRecipients, ...
                    ['Universal Monitor Tool: Stop File Deleted. STARTED (z=',sprintf('%04.0f',thisSlice),')'], ...
                    strcat(LOCLOG,';'));
            end
        end
        if exist(stopFileFnameFocusMonitor,'file')>0 
            if FocusStopFlagCounter == 1
            sendmail(emailRecipients, ...
                    ['Focus Monitor Tool: Stop file created. STOPPED (z=',sprintf('%04.0f',thisSlice),')'], ...
                    strcat(LOCLOG,';'));
            end 
                FocusStopFlagCounter = FocusStopFlagCounter+1;
        else
        FocusStopFlagCounter = 1;
        end

        % Only perform analysis on new slices
        if thisSlice > lastSlice
            
            % Get Directories
            thisSliceDir = fullfile(stackDirTop,stackID,thisSliceSmallDir);
            
            % Initialize Local Log
            t = strcat(datestr(clock,'yyyy-mm-dd-HHMM'));
            LOCLOG = {};
            LOCLOG = [LOCLOG; [t,': Running checks on slice ',num2str(thisSlice)]];
            
            if failsafeActive
                
                % Catch motor movement errors
                [ stopFlagCheck, msgIdentity ] = checkIdentity( thisSliceDir, stopFlagCheck, threshIdentity );
                LOCLOG = [LOCLOG; msgIdentity];
                
                % Catch beam and detector shutoff
                [ stopFlagCheck, msgEntropy ] = checkEntropy( thisSliceDir, stopFlagCheck, threshEntropy );
                LOCLOG = [LOCLOG; msgEntropy];
                
                 % Catch cutting issues
                 if thisSlice > zAlignNumberOfSlices
                     zStackFname = '+cuttingCheck_zStack';
                     pathZStack = fullfile(outputDir,[zStackFname,'.gif']);
                     zAlign(fullfile(stackDirTop,stackID), outputDir, zStackFname, thisSlice-zAlignNumberOfSlices+1, thisSlice, zAlignTileColumn, zAlignTileRow, 1, zAlignCropSize, 3, 0);
                    [ stopFlagCheck, msgCutting ] = checkCutting( pathZStack, stopFlagCheck );
                     LOCLOG = [LOCLOG; msgCutting];
                 end
                 
                % Check status of stop flag and create stop file if flag == 1
                if stopFlagCheck == 1
                    if ~exist(stopFileFnameCheck,'file')>0
                        fid = fopen(stopFileFnameCheck,'w'); fclose(fid);
                    end
                    sendmail(emailRecipients, ...
                        ['Universal Monitor Tool: STOPPED (z=',sprintf('%04.0f',thisSlice),')'], ...
                        strcat(LOCLOG,';'));
                end
                
                % Update log file
                LOCLOG = [LOCLOG; msgEmail];
                GLOBLOG = [GLOBLOG; LOCLOG];
                fid = fopen(fullfile(outputDir,'_log.txt'),'a');
                for i = 1:size(LOCLOG,1)
                    fprintf(fid,'%s \n',LOCLOG{i,:});
                end
                fclose(fid);
                
            end
            
            % Send Status Email
            if thisSlice >= lastSliceEmail + emailSendInt
                
                thisOutputFname = regexprep(thisSliceSmallDir,'(\d{6}).(\d{6})','$1\_$2');
                
                % Create Tableau
                imgTableau = createTableau( thisSliceDir, tableauRefImgPath );
                tableauFname = [thisOutputFname,'_tableau.png'];
                pathTableau = fullfile(outputDir,tableauFname);
                imwrite(imgTableau,pathTableau);
                
                % Create stitched z-stack
                zStackFname = [thisOutputFname,'_zStack'];
                GIFOutname = zAlign_js(fullfile(stackDirTop,stackID), outputDir, zStackFname, thisSlice-zAlignNumberOfSlices+1, thisSlice, zAlignTileColumn, zAlignTileRow, 1, zAlignCropSize, 3, 0);
                % Get latest AF Graph
                lastGraphSubDir=fullfile(AFDir,numberformatter(thisSlice-mod(thisSlice,3)));
                if ~isempty(dir(fullfile(lastGraphSubDir,'*.png')))
                    pathAFgraph = fullfile(lastGraphSubDir,'AFgraph.png');
                else
                    pathAFgraph = fullfile(AFDir,numberformatter(thisSlice-(mod(thisSlice,3)+3)),'AFgraph.png');
                end
                
                % Send Status Update Email
                sendmail(emailRecipients, ...
                    ['Universal Monitor Tool: OK (z=',sprintf('%04.0f',thisSlice),')'], ...
                    strcat(LOCLOG,';'), ...
                    {pathTableau, GIFOutname{:}, pathAFgraph});
                
                lastSliceEmail = thisSlice;
            end
            
            lastSlice = thisSlice;
            disp(LOCLOG)
            
        end
        pause(20);
        
    catch err
        disp(err)
        
    end
end


